## A collection of Bash scripts. 


| NO | Status| Name| About|
|----|:------:|:---------:|----------|
|1|[![Build Status](https://travis-ci.org/Reborns/bash_scripts.svg?branch=master)](https://travis-ci.org/Reborns/bash_scripts) | [get_oracle_jdk_linux_x64.sh](get_oracle_jdk_linux_x64.sh) | Download and install Oracle JDK package, supported versions are JDK 8, JDK 10.
|2| *@* | [get_hеtb_fastest_ip.sh](get_hеtb_fastest_ip.sh) | Get the fastest Hurricane Electric Tunnel Broker IP
|3| ~ | ~ | ~ |
|4| ~ | ~ | ~ |





### 00. License & Others

License is Apache-2.0

Please Star and Fork me.

[Issues](https://bitbucket.org/Reborns/bash_scripts/issues) and [pull requests](https://bitbucket.org/Reborns/bash_scripts/pull-requests/) are welcome.

