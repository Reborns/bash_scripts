#!/usr/bin/env bash

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                                               #
#       Download and install Oracle JDK package bash shell script               #
#       Writen by: RBS                                                          #
#       Report bugs to: https://forum.reborns.info                              #
#       Version: 0.4          Released: 21.03.2018                              #
#                                                                               #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#                                                                               #
#    Usage: ./get_oracle_jdk_linux_x64.sh                                       #
#                                                                               #
#    Custom Usage: ./get_oracle_jdk_linux_x64.sh 9 /path/to/install/            #
#                                                                               #
#    jdk_version: 8(default) or 10 AND jdk_install_dir /opt/ (default)          #
#                                                                               #
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

## Changelog

#  Version 0.5 | 13.04.2018 > Removed JDK9 support  

__green() {
    printf '\033[1;31;32m'
    printf -- "%b\\n" "$1"
    printf '\033[0m'
}
__red() {
    printf '\033[1;31;40m'
    printf -- "%b\\n" "$1"
    printf '\033[0m'
}
__yellow() {
    printf '\033[1;33;40m'
    printf -- "%b\\n" "$1"
    printf '\033[0m'
}

# Check if the script is running with root permissions
if [ "$(id -u)" -ne 0 ]; then
    __red "The script must be run as root! (you can use sudo)"
    exit 1
fi
# Check if the script is running on 64 bit OS. 
if ! [ "$(getconf LONG_BIT)" = 64 ]
then
    echo
    __red "This is a not an x86_64 OS, make sure to run this script on 64 bit OS ... "
    echo
    exit 1
fi
# Check if the curl installed .
if [ ! -x /usr/bin/curl ] ; then
    command -v curl >/dev/null 2>&1 || { echo >&2 "Please install curl or set it in your path. Aborting."; exit 1; }
fi

jdk_version=${1:-8}
jdk_install_dir=${2:-/opt}
JDK_LOCATION=$jdk_install_dir/java

if [[ "$jdk_version" != "8" && "$jdk_version" != "10" ]]; then
    echo
    __red "   Wrong version number !!!   "
    echo
    __green "   Version can be only [ 8 ] or [ 10 ]   "
    echo
  exit 2
fi

if [ ! -d "$jdk_install_dir" ]; then
  if mkdir -p "$jdk_install_dir" > /dev/null 2>&1 ;then
    echo
   __green "OK --- Directory created --- "
    echo 
else
    echo
    __red "FAILED"
    echo
    __red "Provided path is not valid directory: $jdk_install_dir"
    echo
    exit 1
 fi
fi

cd "$jdk_install_dir" || exit

readonly url="http://www.oracle.com"
readonly jdk_url_1="$url/technetwork/java/javase/downloads/index.html"
readonly jdk_url_2=$(
    curl -s $jdk_url_1 | \
    grep -E -o "\\/technetwork\\/java/\\javase\\/downloads\\/jdk${jdk_version}-downloads-.+?\\.html" | \
    head -1 | \
    cut -d '"' -f 1
)
[[ -z "$jdk_url_2" ]] && __red "Could not get jdk download url - $jdk_url_1" >> /dev/stderr

readonly jdk_url_3="${url}${jdk_url_2}"
readonly jdk_url_4=$(
    curl -s "$jdk_url_3" | \
    grep -E -o "http\\:\\/\\/download.oracle\\.com\\/otn-pub\\/java\\/jdk\\/[0-9](u[0-9]+|\\+|[.0-9]+\\+).*\\/jdk-${jdk_version}.*(-|_)linux-(x64|x64_bin).tar.gz" | head -n1
)
JDK_ARCHIVE=$(sed  's:.*/::' <<< "$jdk_url_4")

if ! [ -f "$JDK_ARCHIVE" ];
  then
    __green "Downloading $JDK_ARCHIVE ... "
    echo
    curl -O -j -k -L -H  "Cookie: oraclelicense=accept-securebackup-cookie"  "$jdk_url_4"
else
    echo 
    __yellow  "Archive file $JDK_ARCHIVE already exist !!!"
    echo
  exit 1
fi

JDF=$(sed -e s/-8u/1.8.0_/g -e 's/-linux-x64.tar.gz//g;s/_linux-x64_bin.tar.gz//g' <<< "$JDK_ARCHIVE")

if [ -d "$JDF" ];
    then 
    __yellow "Java version $JDF is already installed "
    echo
    exit 1
fi
if [ -L java ];
   then rm java ;
fi

# Is the file a valid archive?
echo -n "Validating the archive file... "
if gunzip -t "$JDK_ARCHIVE" >/dev/null 2>&1 ; then
    __green "OK"
else
    __red "FAILED"
    echo 
    __yellow "Provided file is not a valid .tar.gz archive: $JDK_ARCHIVE"
    echo
    __yellow "Be sure to download Linux .tar.gz package from the Oracle website"
    echo "$jdk_url_4"
    echo 
    exit 1
fi

# Extract the archive
echo -n "Extracting the archive... and creating symlink... "
tar -xf "$JDK_ARCHIVE" -C "$jdk_install_dir"
rm "$JDK_ARCHIVE"
chown -R root:root "$JDF"
ln -s "$JDF" java
__green "OK"

# Update /etc/profile
echo -n "Updating /etc/profile ... "
cat >> /etc/profile <<EOF
JAVA_HOME=$JDK_LOCATION
PATH=$PATH:$JDK_LOCATION/bin
export JAVA_HOME
export PATH
EOF
__green "OK"

# Update system to use Oracle Java by default
echo -n "Updating system alternatives... "
{
update-alternatives --install "/usr/bin/java" "java" "$JDK_LOCATION/bin/java" 1 
update-alternatives --install "/usr/bin/javac" "javac" "$JDK_LOCATION/bin/javac" 1 
update-alternatives --install "/usr/bin/jps" "jps" "$JDK_LOCATION/bin/jps" 1 
update-alternatives --set java "$JDK_LOCATION"/bin/java 
update-alternatives --set javac "$JDK_LOCATION"/bin/javac 
update-alternatives --set jps "$JDK_LOCATION"/bin/jps 
} >> /dev/null 2>&1
__green "OK"

# Verify and exit installation
echo -n "Verifying Java installation... "
JAVA_CHECK=$(java -version 2>&1)
if [[ "$JAVA_CHECK" == *"Java(TM) SE Runtime Environment"* ]]; then
    __green "OK"
    echo
    __green "Java is successfully installed!"
    echo
    java -version
    echo
    exit 0
else
    __red "FAILED"
    echo
    __red "Java installation failed!"
    echo
    exit 1
fi


